using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LogicScript : MonoBehaviour
{
    public int playerScore;
    public Text scoreText;
    public GameObject gameOverScreen;
    public bool isFinished = false;
    public AudioSource dingsfx;

    [ContextMenu("Increase Score")]
    public void addScore(int scoreToAdd)
    {   
        if (!isFinished) 
        {
            playerScore += scoreToAdd;
            scoreText.text = playerScore.ToString();   
            dingsfx.Play();
        }
    }

    public void restartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void gameOver() 
    {
        gameOverScreen.SetActive(true);
        isFinished = true;
    }
}
