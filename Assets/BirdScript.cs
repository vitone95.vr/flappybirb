using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdScript : MonoBehaviour
{
    public Rigidbody2D myRigibody2D;
    public float flapStrength;
    public LogicScript logic;
    public bool birdAlive = true;
    public AudioSource sadsfx;
    public bool isPlayed = false;
    public AudioSource bg;
    public wingScript wing;

    // Start is called before the first frame update
    void Start()
    {
        logic = GameObject.FindGameObjectWithTag("Logic").GetComponent<LogicScript>();
        wing = GameObject.FindGameObjectWithTag("wing").GetComponent<wingScript>();
        bg.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && birdAlive == true)
        {
            myRigibody2D.velocity = Vector2.up * flapStrength;
            wing.FlipTrigger();
        }

        if (transform.position.y > 17 || transform.position.y < -16) 
        { 
            birdDeath();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        birdDeath();
    }

    public void birdDeath()
    {
        logic.gameOver();
        birdAlive = false;
        if (!isPlayed) 
        {
            bg.Stop();
            sadsfx.Play();
            isPlayed = true;
        }
    }
}
